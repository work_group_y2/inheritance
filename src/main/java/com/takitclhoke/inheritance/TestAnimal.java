/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.takitclhoke.inheritance;

/**
 *
 * @author ทักช์ติโชค
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();

        Dog to = new Dog("To", "Brown");
        dang.speak();
        dang.walk();

        Dog mome = new Dog("Mome", "White");
        dang.speak();
        dang.walk();

        Dog bat = new Dog("Bat", "White");
        dang.speak();
        dang.walk();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();

        Duck zom = new Duck("Zom", "Yello");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck gabgab = new Duck("GabGab", "Black");
        zom.speak();
        zom.walk();
        zom.fly();

        System.out.println("Zom is Animal: " + (zom instanceof Animal));
        System.out.println("Zom is Duck: " + (zom instanceof Duck));
        System.out.println("Zom is Cat: " + (zom instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Animal: " + (animal instanceof Animal));

        System.out.println("All animals");
        Animal[] animals = {dang, to, mome, bat, zero, zom, gabgab};
        for (int i = 0; i < animals.length; i++) {
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck) {
                Duck duck = (Duck) animals[i];
                duck.fly();
            }
        }

    }

}
